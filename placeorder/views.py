from django.shortcuts import render
from .models import Order, OrderItem, Cart
from inventory.models import InventoryItems
# Create your views here.


def index(request):
    context = {}
    if 'admin_type' not in request.session:
        return render(request, 'account/sign-in.html', context)
    admin_type = request.session['admin_type']
    if admin_type == 1:
        context.update({'admin_type': admin_type})
    if 'user_id' not in request.session:
        return render(request, 'account/sign-in.html', context)
    all_items = InventoryItems.objects.filter(user_id=request.session['user_id'])
    # TODO RETURN VARIABLE
    all_carts = Cart.objects.all()
    if not all_carts:
        context.update({'all_items': all_items})
    else:
        billTotalValue = 0
        for item in all_carts:
            billTotalValue = billTotalValue + item.cart_item_total
            context.update({'all_items': all_items, 'all_carts': all_carts, 'billTotalValue': billTotalValue})
    return render(request, 'placeorder/index.html', context)


def add_selected_item(request):
    context = {}
    if 'admin_type' not in request.session:
        return render(request, 'account/sign-in.html', context)
    admin_type = request.session['admin_type']
    if admin_type == 1:
        context.update({'admin_type': admin_type})
    inventory_item = InventoryItems.objects.all()
    for item_contain in inventory_item:
        if item_contain.item_qty == 0:
            item_contain.delete()
    all_items = InventoryItems.objects.all()
    if int(request.POST['current-qty']) > 0:
        current_ordered_qty = int(request.POST['current-qty'])
        current_ordered_item_id = int(request.POST['order-item'])
        inventory_item = InventoryItems.objects.filter(pk=current_ordered_item_id)
        if inventory_item:
            for item_contain in inventory_item:
                item_id = item_contain.id
                item_name = item_contain.item_name
                item_price = item_contain.item_price
                item_qty = item_contain.item_qty
                all_carts = Cart.objects.filter(cart_item_id=item_id)
                if not all_carts:
                    availablitycheck = 0
                else:
                    availablitycheck = 1
                if availablitycheck == 1:
                    used_ordered_qty = 0
                    for cart_item in all_carts:
                        used_ordered_qty = used_ordered_qty + cart_item.cart_item_qty
                    item_qty = item_qty - used_ordered_qty
            if item_qty >= current_ordered_qty:
                new_cart = Cart()
                new_cart.cart_item_id = current_ordered_item_id
                new_cart.cart_item_name = item_name
                new_cart.cart_item_qty = int(current_ordered_qty)
                new_cart.cart_item_price = int(item_price)
                new_cart.cart_item_total = int(item_price * current_ordered_qty)
                new_cart.save()
            else:
                context.update({'successresponse': "Sufficient Item Not Available", 'max': int(item_qty)})
    else:
        context.update({'successresponse': "Invalid Request"})

    # TODO RETURN VARIABLE
    all_items = InventoryItems.objects.filter(user_id=request.session['user_id'])
    all_carts = Cart.objects.all()
    if not all_carts:
        context.update({'all_items': all_items})
    else:
        billTotalValue = 0
        for item in all_carts:
            billTotalValue = billTotalValue + item.cart_item_total
            context.update({'all_items': all_items, 'all_carts': all_carts, 'billTotalValue': billTotalValue})
    return render(request, 'placeorder/index.html', context)


def delete_update(request):
    context = {}
    if 'admin_type' not in request.session:
        return render(request, 'account/sign-in.html', context)
    admin_type = request.session['admin_type']
    if admin_type == 1:
        context.update({'admin_type': admin_type})
    # TODO Delete CART ITEM
    if 'delete' in request.POST:
        Cart.objects.filter(pk=request.POST['delete']).delete()
        context.update({'successresponse': 'Deleted Succesfully'})
    else:
        context.update({'successresponse': 'Invalid Request'})

    # TODO RETURN VARIABLE
    all_carts = Cart.objects.all()

    if not all_carts:
        all_items = InventoryItems.objects.filter(user_id=request.session['user_id'])
        context = context.update({'all_items': all_items})
    else:
        billTotalValue = 0
        for item in all_carts:
            billTotalValue = billTotalValue + item.cart_item_total
        all_items = InventoryItems.objects.filter(user_id=request.session['user_id'])
        context.update({'all_items': all_items, 'all_carts': all_carts, 'billTotalValue': billTotalValue})
    return render(request, 'placeorder/index.html', context)


def operation_cart(request):
    context = {}
    if 'admin_type' not in request.session:
        return render(request, 'account/sign-in.html', context)
    admin_type = request.session['admin_type']
    if admin_type == 1:
        context.update({'admin_type': admin_type})

    # TODO DELETE CART
    if 'delete' in request.POST:
        Cart.objects.all().delete()
        context.update({'successresponse': 'Deleted Cart Succesfully'})
    # TODO GENERATE BILL
    elif 'generate' in request.POST:
            all_carts = Cart.objects.all()
            if not all_carts:
                context.update({'successresponse': 'add items to cart'})
            else:
                user_id = request.session['user_id']
                new_order = Order()
                new_order.seller_id = user_id
                billTotalValue = 0
                for item in all_carts:
                    billTotalValue = billTotalValue + item.cart_item_total
                new_order.order_price = billTotalValue
                new_order.save()
                for item in all_carts:
                    order_item_total = int(item.cart_item_price) * int(item.cart_item_qty)
                    new_orderitem = OrderItem.objects.create(order_id=new_order, user_id=user_id,
                                                             order_item_name=item.cart_item_name,
                                                             order_item_qty=item.cart_item_qty,
                                                             order_item_price=item.cart_item_price,
                                                             order_item_total=order_item_total)

                    inventoryitem = InventoryItems.objects.filter(id=item.cart_item_id)
                    for inventory_contain in inventoryitem:
                        inventory_contain.item_qty = inventory_contain.item_qty - item.cart_item_qty
                        inventory_contain.save()
                    new_orderitem.save()
                Cart.objects.all().delete()
                context.update({'successresponse': 'Bill Generated Successful'})
    # TODO END OF GENERATE BILL
    else:
        context.update({'successresponse': 'Invalid Request'})

    # TODO RETURN VARIABLE
    all_carts = Cart.objects.all()
    if not all_carts:
        all_items = InventoryItems.objects.filter(user_id=request.session['user_id'])
        context.update({'all_items': all_items})
    else:
        billTotalValue = 0
        for item in all_carts:
            billTotalValue = billTotalValue + item.cart_item_total
        all_items = InventoryItems.objects.filter(user_id=request.session['user_id'])
        context.update({'all_items': all_items, 'all_carts': all_carts, 'billTotalValue': billTotalValue})
    return render(request, 'placeorder/index.html', context)


def order_view(request):
    context = {}
    if 'admin_type' not in request.session:
        return render(request, 'account/sign-in.html', context)
    admin_type = request.session['admin_type']
    if admin_type == 1:
        context.update({'admin_type': admin_type})
    user_id = request.session['user_id']
    orders = Order.objects.filter(seller_id=user_id)
    if orders:
        context.update({'orders': orders})
    return render(request, 'placeorder/orderlist.html', context)


def order_delete(request):
    context = {}
    if 'admin_type' not in request.session:
        return render(request, 'account/sign-in.html', context)
    admin_type = request.session['admin_type']
    if admin_type == 1:
        context.update({'admin_type': admin_type})
    if 'delete' in request.POST:
        Order.objects.filter(pk=request.POST['delete']).delete()
        OrderItem.objects.filter(order_id=request.POST['delete']).delete()
        context.update({'successresponse': 'Deleted Order'})
    elif 'order_id' in request.POST:
        if 'order_id' in request.POST:
            order_id = request.POST['order_id']
            order_item = OrderItem.objects.filter(order_id=order_id)
        if order_item:
            context.update({'order_item': order_item})
        return render(request, 'placeorder/orderitemlist.html', context)

    user_id = request.session['user_id']
    orders = Order.objects.filter(seller_id=user_id)
    if orders:
        context.update({'orders': orders})
    return render(request, 'placeorder/orderlist.html', context)


def order_item_view(request):
    context = {}
    if 'admin_type' not in request.session:
        return render(request, 'account/sign-in.html', context)
    admin_type = request.session['admin_type']
    if admin_type == 1:
        context.update({'admin_type': admin_type})
    user_id = request.session['user_id']
    order_item = OrderItem.objects.filter(user_id=user_id)
    if order_item:
        context.update({'order_item': order_item})
    return render(request, 'placeorder/orderitemlist.html', context)
