from django.db import models
# Create your models here.


# ToDO Order Table
class Order(models.Model):
    seller_id = models.PositiveSmallIntegerField()
    order_price = models.PositiveSmallIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.seller_id) + ' - ' + str(self.order_price)


# ToDO Order Item Table
class OrderItem(models.Model):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    user_id = models.PositiveSmallIntegerField()
    order_item_name = models.CharField(max_length=90)
    order_item_qty = models.PositiveSmallIntegerField()
    order_item_price = models.PositiveSmallIntegerField()
    order_item_total = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.order_item_name + ' - ' + str(self.order_item_price)


# ToDO Current CartTable
class Cart(models.Model):
    cart_item_id = models.CharField(max_length=90)
    cart_item_name = models.CharField(max_length=90)
    cart_item_qty = models.PositiveSmallIntegerField()
    cart_item_price = models.PositiveSmallIntegerField()
    cart_item_total = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.cart_item_name + ' - ' + str(self.cart_item_price)
