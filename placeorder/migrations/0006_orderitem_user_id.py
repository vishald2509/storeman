# Generated by Django 2.1 on 2018-11-18 19:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('placeorder', '0005_auto_20181116_1513'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitem',
            name='user_id',
            field=models.PositiveSmallIntegerField(default=1),
        ),
    ]
