from django.urls import re_path
from . import views

app_name = 'placeorder'


urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^add-selected-item$', views.add_selected_item, name='add_selected_item'),
    re_path(r'^operation$', views.delete_update, name='operation'),
    re_path(r'^operation-cart$', views.operation_cart, name='operation-cart'),
    re_path(r'^order-view$', views.order_view, name="order-view"),
    re_path(r'^order-delete$', views.order_delete, name="order-delete"),
    re_path(r'^order-item-list$', views.order_item_view, name="order-item-view"),
]
