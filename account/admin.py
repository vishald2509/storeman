from django.contrib import admin
from .models import StoreAccount

# Register your models here.

admin.site.register(StoreAccount)
