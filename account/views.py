from django.http import HttpResponse
from django.shortcuts import render
from .models import StoreAccount
from inventory.views import index as inventory_index
from welcome.views import index as welcome
from placeorder.models import Cart
# Create your views here.


def index(request):
    all_store_account = StoreAccount.objects.all()
    html = ''
    for store_account in all_store_account:
        url = 'account/' + str(store_account.id) + '/'
        html += '<a href="' + url + '">' + store_account.first_name + ' ' + store_account.last_name + '</a><br>'
    return HttpResponse(html)


def detail(request, store_account_id):
    return HttpResponse("<h3>Details for StoreAccount id: " + str(store_account_id) + " </h3>")


def sign_in(request):
    if 'user_id' in request.session:
        return welcome(request)
    context = {}
    return render(request, 'account/sign-in.html', context)


def sign_up(request):
    if 'user_id' in request.session:
        return welcome(request)
    context = {}
    return render(request, 'account/sign-up.html', context)


def sign_in_validate(request):

    if 'username' in request.POST and 'password' in request.POST:
        flag = 0
        all_store_account = StoreAccount.objects.all()
        for account in all_store_account:
            if request.POST['username'] == account.username:
                flag = 1
                break
        if flag == 0:
            context = {'error_message': '*Invalid Username'}
        elif flag == 1:
            if request.POST['password'] == account.password:
                request.session['user_id'] = account.user_id
                request.session['admin_type'] = account.admin_type
                return inventory_index(request)
            else:
                context = {'error_message': '*Invalid Password'}
    else:
        context = {'error_message': '*Enter Valid UserName and Password'}
    return render(request, 'account/sign-in.html', context)


def sign_up_validate(request):
    context = {}
    all_store_account = StoreAccount.objects.all()
    flag = 0
    for account in all_store_account:
        if request.POST['username'] == account.username:
            flag = 1
            break
    if request.POST['password'] == request.POST['confirmPassword']:
        if flag == 0:
            if 'admin_type' in request.POST:
                admin_type = request.POST['admin_type']
            else:
                admin_type = '1'
            first_name = request.POST['firstName']
            last_name = request.POST['lastName']
            username = request.POST['username']
            password = request.POST['password']
            gender = request.POST['gender']
            store_account = StoreAccount()
            store_account.first_name = first_name
            store_account.last_name = last_name
            store_account.username = username
            store_account.password = password
            store_account.gender = gender
            store_account.admin_type = admin_type
            store_account.save()
            if 'user_id' in request.session:
                user_id = request.session['user_id']
            else:
                user_id = store_account.pk
            store_account.user_id = user_id
            store_account.save()
            context = {'user_detail': store_account}
            return logout(request)
        elif flag == 1:
            context = {'error_message': '*Username Already taken'}
    else:
        context = {'error_message': "Password Fields don't match"}
    return render(request, 'account/sign-up.html', context)


def slave_signup(request):
    context = {}
    return render(request, 'account/slave-signup.html', context)


def logout(request):
    Cart.objects.all().delete()
    request.session.clear()
    return welcome(request)
