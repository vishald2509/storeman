from django.urls import re_path
from . import views

app_name = 'account'
urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^(?P<store_account_id>[0-9]+)$', views.detail, name='detail'),
    re_path(r'^sign-in$', views.sign_in, name='sign-in'),
    re_path(r'^sign-up$', views.sign_up, name='sign-up'),
    re_path(r'^slave-signup$', views.slave_signup, name='slave_signup'),
    re_path(r'^sign_in_validate$', views.sign_in_validate, name='sign_in_validate'),
    re_path(r'^sign_up_validate$', views.sign_up_validate, name='sign_up_validate'),
    re_path(r'^logout$', views.logout, name='logout'),
]

