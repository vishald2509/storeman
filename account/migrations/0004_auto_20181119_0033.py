# Generated by Django 2.1 on 2018-11-18 19:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_storeaccount_admin_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storeaccount',
            name='admin_type',
            field=models.PositiveSmallIntegerField(),
        ),
    ]
