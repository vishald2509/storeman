# Generated by Django 2.1 on 2018-11-18 20:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0005_storeaccount_user_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storeaccount',
            name='user_id',
            field=models.PositiveSmallIntegerField(default=1),
        ),
    ]
