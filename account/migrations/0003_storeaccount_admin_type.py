# Generated by Django 2.1 on 2018-11-18 18:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20181028_1835'),
    ]

    operations = [
        migrations.AddField(
            model_name='storeaccount',
            name='admin_type',
            field=models.PositiveSmallIntegerField(default='1'),
        ),
    ]
