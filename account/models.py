from django.db import models

# Create your models here.


# ToDo  StoreAccount Table


class StoreAccount(models.Model):
    first_name = models.CharField(max_length=90)
    last_name = models.CharField(max_length=90)
    username = models.CharField(max_length=90)
    password = models.CharField(max_length=90)
    gender = models.BooleanField()
    admin_type = models.PositiveSmallIntegerField()
    user_id = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.first_name + '-' + self.last_name
