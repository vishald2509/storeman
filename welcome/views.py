from django.shortcuts import render
from inventory.views import index as welcome
# Create your views here.


def index(request):
    if 'user_id' in request.session:
        return welcome(request)
    else:
        context = {}
        return render(request, 'welcome/index.html', context)

