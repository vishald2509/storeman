from django.shortcuts import render, HttpResponseRedirect
from .models import InventoryItems, StoreAccount
from placeorder.views import index as generate_bill_view

# Create your views here.


def index(request):
    context = {}
    if 'admin_type' not in request.session:
        return render(request, 'account/sign-in.html', context)
    admin_type = request.session['admin_type']
    if admin_type == 1:
        context.update({'admin_type': admin_type})
    else:
        context.update({'admin_slave': 'slave'})
    if 'successresponse' in request.GET:
        successresponse = request.GET['successresponse']
        context.update({'successresponse': successresponse})
    if 'user_id' not in request.session:
        return render(request, 'account/sign-in.html', context)
    user_id = request.session['user_id']
    all_items = InventoryItems.objects.all()
    for item_contain in all_items:
        if item_contain.item_qty == 0:
            item_contain.delete()
    all_items = InventoryItems.objects.filter(user_id=user_id)
    context.update({'all_items': all_items, 'user_id': user_id})
    return render(request, 'inventory/index.html', context)


def delete_update(request):
    context = {}
    if 'admin_type' not in request.session:
        return render(request, 'account/sign-in.html', context)
    admin_type = request.session['admin_type']
    if admin_type == 1:
        context.update({'admin_type': admin_type})
    if 'delete' in request.POST:
        InventoryItems.objects.filter(pk=request.POST['delete']).delete()
        return index(request)
    elif 'update' in request.POST:
        item = InventoryItems.objects.filter(pk=request.POST['update'])
        for item_contain in item:
            item_name= item_contain.item_name
            item_price= item_contain.item_price
            item_qty= item_contain.item_qty
        context = {
            'item_name': item_name,
            'item_price': item_price,
            'item_qty': item_qty,
            'item_id': request.POST['update']
        }
        return render(request, 'inventory/update.html', context)
    elif 'add' in request.POST:
        user_id = request.session['user_id']

        user = StoreAccount.objects.get(pk=user_id)
        item_qty = request.POST['item_qty']
        item_price = request.POST['item_price']
        item_name = request.POST['item_name']
        new_inventory_item = InventoryItems.objects.create(user_id=user, item_name=item_name, item_qty=item_qty, item_price=item_price)
        new_inventory_item.save()
        context.update({'successresponse': 'inventory item added'})
        return render(request, 'inventory/add-inventory.html', context)
    else:
        return index(request)


def update_inventory(request):
    successresponse = ""
    if 'update' in request.POST:
        item_id = request.POST['item_id']
        item_name = request.POST['item_name']
        item_qty = request.POST['item_qty']
        item_price = request.POST['item_price']
        inventory_obj = InventoryItems.objects.filter(pk=item_id)
        for inventory_item in inventory_obj:
            inventory_item.item_name = item_name
            inventory_item.item_qty = item_qty
            inventory_item.item_price = item_price
            inventory_item.save()
        successresponse = "update successfully"
    else:
        successresponse = "Error Update Failed"
    return HttpResponseRedirect('/inventory/?successresponse='+successresponse)


def add_inventory(request):
    context = {}
    if 'admin_type' not in request.session:
        return render(request, 'account/sign-in.html', context)
    admin_type = request.session['admin_type']
    if admin_type == 1:
        context.update({'admin_type': admin_type})
    return render(request, 'inventory/add-inventory.html', context)