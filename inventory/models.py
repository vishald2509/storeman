from django.db import models
from account.models import StoreAccount

# Create your models here.

# ToDo  StoreAccount Table


class InventoryItems(models.Model):
    user_id = models.ForeignKey(StoreAccount, on_delete=models.CASCADE)
    item_name = models.CharField(max_length=90)
    item_qty = models.PositiveSmallIntegerField()
    item_price = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.item_name + ' - ' + str(self.item_qty)
