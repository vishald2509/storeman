from django.urls import re_path
from . import views

app_name = 'inventory'


urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^operation$', views.delete_update, name='operation'),
    re_path(r'^add-inventory$', views.add_inventory, name='add-inventory'),
    re_path(r'^update-inventory$', views.update_inventory, name='update-inventory'),
]
