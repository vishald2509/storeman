

##StoreMan
**------------------------------------------------------------------------------------------------------------**

* StoreMan is Web Application Built using HTML and Python. It consists of user friendly Web based environment UI.
* Where users (Store Managers) can easily interact and Manage store.
* There are many functionality consists of Billing, Inventory Management and expenditures views.
* The Administrator can get good glance over the store analytics which will help him to get better understanding of his business and estimate future of his business.

**------------------------------------------------------------------------------------------------------------**

##Module :
**--------------------------------**

1. Login
2. Registration
3. Inventory
4. Billing

**------------------------------------------------------------------------------------------------------------**

##Built Using:
**--------------------------------**

* Django Framwork
* Programing Language Python
* Database sqlite3
* HTML, CSS, BOOTSTRAP.